from flask import Flask
from flask.ext.ldap import LDAP, login_required


app = Flask(__name__)
app.debug = True

app.config['LDAP_HOST'] = 'ad.dneg.com'
app.config['LDAP_DOMAIN'] = 'dneg.com'
app.config['LDAP_SEARCH_BASE'] = 'CU=People, DC=ad, DC=dnet, DC=com'
app.config['LDAP_LOGIN_VIEW'] = 'login'
ldap = LDAP(app)
app.secret_key = "jdfhgsjdhcjshdbfg,jshg,jhsbf,jdhfvjhsg"
app.add_url_rule('/login', 'login', ldap.login, methods=['GET', 'POST'])



@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    app.run()
